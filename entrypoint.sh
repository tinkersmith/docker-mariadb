#!/bin/bash
#
# Check if MySQL has been initialized, and create databases \w user if needed.
#
# NOTE: That once a database is initialized, it will not update to changes
# with the MYSQL_DATABASE or MYSQL_USER environment variables.
#

set -e

#
# Add databases specified by the environment arguments.
#
# Specify a list of databases by providing a comma separate list of database
# name in the ${MYSQL_DATABASE_LIST} variable. This image also supports a single
# database specified by using ${MYSQL_DATABASE} variable instead.
#
function addDatabases {
	local dbNames=""
	local dbs=""
	local users=""

	# Create the databases if one there are any specified.
	if [ -n "${MYSQL_DATABASE_LIST}" ]; then
		# Start reading the manifest file one line at a time.
		IFS=',' read -ra DB_INFO <<< "${MYSQL_DATABASE_LIST}"

		for db in ${DB_INFO[@]}; do
			dbUser="${db}_MYSQL_USER"
			dbPass="${db}_MYSQL_PASSWORD"

			if ! [ -d "/var/lib/mysql/${db}" ]; then
				addSnippet "${db}" "${!dbUser:-${MYSQL_USER}}" "${!dbPass:-${MYSQL_PASSWORD}}"
			fi
		done
	elif ! [ -z "$MYSQL_DATABASE" ]; then
		addSnippet "$MYSQL_DATABASE" "$MYSQL_USER" "$MYSQL_PASSWORD"
	fi

	# If any databases need to be created, generate them from the script.
	if ! [ -z "${dbs}" ]; then
		echo -e "\e[32m[i] Creating databases: ${dbnames}"
		echo "${dbs}
FLUSH PRIVILEGES;
${users}" | mysqld --user=mysql --bootstrap --skip-networking --verbose=0
	fi
}

#
# Generate the add database query to the initialization script.
#
# This appends the generation of multiple database create and user grant
# statements into a larger aggregated script that executes in one database
# instance.
#
function addSnippet {
	if ! [ -d "/var/lib/mysql/${1}" ]; then
			dbNames="{$dbNames}, ${1}"
			dbs="${dbs}
CREATE DATABASE IF NOT EXISTS \`${1}\` CHARACTER SET utf8 COLLATE utf8_general_ci;
"
		# Create user account if information is available.
		if ! [ -z "${2}" ] && ! [ -z "${3}" ]; then
			users="${users}
CREATE USER IF NOT EXISTS '${2}'@'%' IDENTIFIED BY '${3}';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON \`${1}\`.* TO '${2}'@'%';
"
		fi
	fi
}


# ====================================
# Main script
# ====================================
if ! [ -d "/run/mysqld" ]; then
	mkdir -p /run/mysqld;
	chown -R mysql:mysql /run/mysqld;
fi

if ! [ -d /var/lib/mysql/mysql ]; then
	echo -e "\e[32m[i] MySQL data directory not found, initializing databases\e[0m"

	chown -R mysql:mysql /var/lib/mysql

	mysql_install_db --user=mysql --rpm --ldata=/var/lib/mysql > /dev/null
	query="DROP DATABASE IF EXISTS \`test\`;"

	if ! [ -z "$MYSQL_ROOT_PASSWORD" ]; then
		query="${query}
USE mysql;
FLUSH PRIVILEGES;
CREATE USER IF NOT EXISTS 'root'@'%';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' identified by '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' identified by '' WITH GRANT OPTION;
"
	fi

	echo "$query" | mysqld --user=mysql --bootstrap --skip-networking --verbose=0
fi

addDatabases

exec /usr/bin/mysqld --user=mysql --log-warnings=1
