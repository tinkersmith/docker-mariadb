#!/bin/bash
#
# Script to build and push a new version of this Docker image.
#

# TODO: Add simple build options to allow building of different environments.
DEV=0
TAG="10.11.6"

if ! [ -z "${DEV}" ] && [ "${DEV}" = "1" ]; then
  TAG="${TAG}-dev"
fi

function main() {
  docker build \
    --build-arg "MYSQL_SIZE=medium" \
    -t "tinkersmith/mariadb" \
    ./

  docker tag "tinkersmith/mariadb" "tinkersmith/mariadb:${TAG}"
}

main "$@"
