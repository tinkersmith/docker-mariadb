FROM alpine:3.19

ARG MYSQL_SIZE=medium

RUN apk add --update --no-cache bash mariadb mariadb-client mariadb-server-utils

# Move all configurations on the server so they can be used by
# any containers that use this image.
ADD "conf.d" "/root/conf.d"
ADD "createdb.sh" "/usr/sbin/createdb"

# Move a default configuration into place.
RUN chmod -R ugo-wx "/root/conf.d" && \
  cp "/root/conf.d/${MYSQL_SIZE}.cnf" "/etc/my.cnf.d/mariadb-server.cnf"

# Add and call the entrypoint script.
ADD entrypoint.sh entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
