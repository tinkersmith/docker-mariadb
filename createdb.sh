#!/bin/bash
#
# Create databases on this MariaDB instance.
#

#
# Create a new database.
#
function create {
	# Create the database if a database is specified but not already created.
	if ! [ -z "${1}" ] && ! [ -d "/var/lib/mysql/${1}" ]; then
		local db=$1
		local user=$2
		local pass=$3

		echo -e "\e[93m[i] Creating database: ${db}\e[0m"
		local query="
CREATE DATABASE IF NOT EXISTS \`${db}\` CHARACTER SET utf8 COLLATE utf8_general_ci;
"

		# Create user account if information is available.
		if ! [ -z "${user}" ] && ! [ -z "${pass}" ]; then
			echo -e "\e[93m[i] Adding user: ${user}\e[0m";
			local query="${query}
FLUSH PRIVILEGES;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON \`${db}\`.* TO '${user}'@'%' IDENTIFIED BY '${pass}';
"
		fi

		# Is MySQL daemon already runnning?
		local pid=$(pidof mysqld)

		if [ -z "$pid" ]; then
			echo "${query}" | mysqld --user=mysql --bootstrap --skip-networking --verbose=0
		else
			echo "${query}" | mysql
		fi
	fi
}

# Execute the script.
create "$@"
