# MariaDB

A simple MariaDB server on a Alpine Linux OS. Meant to be small and compact,
but still support some flexibility of MariaDB settings and databases.


## Creating Databases

This image provides 3 methods for specifying Databases for this MariaDB
instance.

+ 1.) Using environment variables traditionally used by other MySQL containers.
+ 2.) A `manifest.txt` file located in the `/usr/share/mysql/.manifests` folder.
+ 3.) Calling the `/scripts/create-db.sh` script while the container is running.

Each of these methods is describe in more detail below, and has different
benefits depending on the situation. For instance, the `manifest.txt` file was
added to ease the creation of multiple databases when the container is started.

Keep in mind that MySQL has to be running in order for the create database
queries to run. This could make the timing of when to run the `create-db.sh`
difficult to do in an automated fashion.

_All methods only create the database if it does not already exist. If a database
has already been created, the user credentials do not get updated even if they
are changed in the `manifest.txt` or environment variables. There maybe a
future improvement for this, but for now this will need to get updated using
SQL queries directly to the MariaDB service._


### Environment Variables

### Manifest file

### create-db.sh script
